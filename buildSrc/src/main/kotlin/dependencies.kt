@file:Suppress("unused") // usages in build scripts are not tracked properly

import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.artifacts.ProjectDependency
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.tasks.AbstractCopyTask
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import java.io.File


fun Project.commonDep(coord: String): String {
    val parts = coord.split(':')
    return when (parts.size) {
        1 -> "$coord:$coord:${commonVer(coord, coord)}"
        2 -> "${parts[0]}:${parts[1]}:${commonVer(parts[0], parts[1])}"
        3 -> coord
        else -> throw IllegalArgumentException("Illegal maven coordinates: $coord")
    }
}

fun customCommonDep(project:Project,coord: String): String{ return project.commonDep(coord) } // to use as intermediatery between build.gradle kotlin method

fun Project.commonDep(group: String, artifact: String, vararg suffixesAndClassifiers: String): String {
    val (classifiers, artifactSuffixes) = suffixesAndClassifiers.partition { it.startsWith(':') }
    return "$group:$artifact${artifactSuffixes.joinToString("")}:${commonVer(group, artifact)}${classifiers.joinToString("")}"
}

fun customCommonDep(project:Project,group: String, artifact: String, suffixesAndClassifiers: List<String>): String{ return project.commonDep(group, artifact,*suffixesAndClassifiers.toTypedArray()) } // to use as intermediatery between build.gradle kotlin method

fun Project.commonVer(group: String, artifact: String) =
    when {
        rootProject.getExtensions().getExtraProperties().has("versions.$artifact") -> rootProject.getExtensions().getExtraProperties()["versions.$artifact"]
        rootProject.getExtensions().getExtraProperties().has("versions.$group") -> rootProject.getExtensions().getExtraProperties()["versions.$group"]
        else -> throw GradleException("Neither versions.$artifact nor versions.$group is defined in the root project's extra")
    }

fun Project.preloadedDeps(
    vararg artifactBaseNames: String,
    baseDir: File = File(rootDir, "dependencies"),
    subdir: String? = null,
    optional: Boolean = false
): ConfigurableFileCollection {
    val dir = if (subdir != null) File(baseDir, subdir) else baseDir
    if (!dir.exists() || !dir.isDirectory) {
        if (optional) return files()
        throw GradleException("Invalid base directory $dir")
    }
    val matchingFiles = dir.listFiles { file -> artifactBaseNames.any { file.matchMaybeVersionedArtifact(it) } }
    if (matchingFiles == null || matchingFiles.size < artifactBaseNames.size) {
        throw GradleException(
            "Not all matching artifacts '${artifactBaseNames.joinToString()}' found in the '$dir' " +
                    "(missing: ${artifactBaseNames.filterNot { request ->
                        matchingFiles.any {
                            it.matchMaybeVersionedArtifact(
                                request
                            )
                        }
                    }.joinToString()};" +
                    " found: ${matchingFiles?.joinToString { it.name }})"
        )
    }
    return files(*matchingFiles.map { it.canonicalPath }.toTypedArray())
}

fun Project.ideaUltimatePreloadedDeps(vararg artifactBaseNames: String, subdir: String? = null): ConfigurableFileCollection {
    val ultimateDepsDir = fileFrom(rootDir, "ultimate", "dependencies")
    return if (ultimateDepsDir.isDirectory) preloadedDeps(*artifactBaseNames, baseDir = ultimateDepsDir, subdir = subdir)
    else files()
}

fun kotlinDep(artifactBaseName: String, version: String): String = "org.jetbrains.kotlin:kotlin-$artifactBaseName:$version"

val Project.useBootstrapStdlib: Boolean get() =
    findProperty("jpsBuild")?.toString() == "true"

fun Project.kotlinStdlib(suffix: String? = null): Any {
    return if (useBootstrapStdlib)
        kotlinDep(listOfNotNull("stdlib", suffix).joinToString("-"), project.findProperty("bootstrapKotlinVersion") as String)
    else
        getDependencies().project(mapOf<String,String>("path" to listOfNotNull(":kotlin-stdlib", suffix).joinToString("-"))) as ProjectDependency
}
fun customKotlinStdlib(project:Project,suffix: String? = null): Any{return project.kotlinStdlib(suffix)}

fun DependencyHandler.projectTests(name: String): ProjectDependency = project(mapOf<String,String>("path" to name, "configuration" to "tests-jar")) as ProjectDependency
fun DependencyHandler.projectRuntimeJar(name: String): ProjectDependency = project(mapOf<String,String>("path" to name, "configuration" to "runtimeJar")) as ProjectDependency
fun DependencyHandler.projectArchives(name: String): ProjectDependency = project(mapOf<String,String>("path" to name, "configuration" to "archives")) as ProjectDependency

fun customProjectTests(dependencyHandler:DependencyHandler,name: String): ProjectDependency {return dependencyHandler.projectTests(name)}
fun customProjectRuntimeJar(dependencyHandler:DependencyHandler,name: String): ProjectDependency {return dependencyHandler.projectRuntimeJar(name)}

val Project.protobufVersion: String get() = findProperty("versions.protobuf") as String

val Project.protobufRepo: String
    get() =
        "https://teamcity.jetbrains.com/guestAuth/app/rest/builds/buildType:(id:Kotlin_Protobuf),status:SUCCESS,pinned:true,tag:$protobufVersion/artifacts/content/internal/repo/"
fun getProtobufRepoFun(project: Project): String {
	return project.protobufRepo
}
fun Project.protobufLite(): String = "org.jetbrains.kotlin.protobuf:protobuf:debian"
fun Project.protobufFull(): String = "org.jetbrains.kotlin.protobuf:protobuf:debian"

fun customProtobufLite(project:Project):String{return project.protobufLite()}
fun customProtobufFull(project:Project):String{return project.protobufFull()}

fun File.matchMaybeVersionedArtifact(baseName: String) = name.matches(baseName.toMaybeVersionedJarRegex())

private val wildcardsRe = """[^*?]+|(\*)|(\?)""".toRegex()

private fun String.wildcardsToEscapedRegexString(): String = buildString {
    wildcardsRe.findAll(this@wildcardsToEscapedRegexString).forEach {
        when {
            it.groups[1] != null -> append(".*")
            it.groups[2] != null -> append(".")
            else -> append("\\Q${it.groups[0]!!.value}\\E")
        }
    }
}

private fun String.toMaybeVersionedJarRegex(): Regex {
    val hasJarExtension = endsWith(".jar")
    val escaped = this.wildcardsToEscapedRegexString()
    return Regex(if (hasJarExtension) escaped else "$escaped(-\\d.*)?\\.jar") // TODO: consider more precise version part of the regex
}


fun Project.firstFromJavaHomeThatExists(vararg paths: String, jdkHome: File = File(this.property("JDK_16") as String)): File? =
    paths.map { File(jdkHome, it) }.firstOrNull { it.exists() }.also {
        if (it == null)
            logger.warn("Cannot find file by paths: ${paths.toList()} in $jdkHome")
    }
fun customFirstFromJavaHomeThatExists(project: Project,paths: List<String>, jdkHome: File = File(project.property("JDK_16") as String)): File?{
	println(project.property("JDK_16") as String)
	return project.firstFromJavaHomeThatExists(*paths.toTypedArray(),jdkHome=jdkHome)
}
fun Project.toolsJar(jdkHome: File = File(this.property("JDK_18") as String)): File? =
    firstFromJavaHomeThatExists("lib/tools.jar", jdkHome = jdkHome)
fun customToolsJar(project:Project):File?{return project.toolsJar()}
val compilerManifestClassPath
    get() = "annotations-13.0.jar kotlin-stdlib.jar kotlin-reflect.jar kotlin-script-runtime.jar trove4j.jar"

object EmbeddedComponents {
    val CONFIGURATION_NAME = "embeddedComponents"
}

fun AbstractCopyTask.fromEmbeddedComponents() {
    val embeddedComponents = project.configurations.getByName(EmbeddedComponents.CONFIGURATION_NAME)
    if (this is ShadowJar) {
        from(embeddedComponents)
    } else {
        dependsOn(embeddedComponents)
        from {
            embeddedComponents.map { file ->
                if (file.isDirectory)
                    project.files(file)
                else
                    project.zipTree(file)
            }
        }
    }
}

fun customFromEmbeddedComponents(abstractCopyTask:AbstractCopyTask){abstractCopyTask.fromEmbeddedComponents()}

